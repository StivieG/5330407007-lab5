<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : twitter.xsl
    Created on : December 6, 2012, 4:14 PM
    Author     : Chowwanat Wikkanesrungsun
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html"/>

    <!-- TODO customize transformation rules 
         syntax recommendation http://www.w3.org/TR/xslt 
    -->
    <xsl:template match="/">
        <html>
            <head>
                <title>twitter.xsl</title>
            </head>
            <body>
                <h1>
                    <xsl:text>Twitter @</xsl:text>
                    <a>
                        <xsl:attribute name="href"> 
                            <xsl:value-of select="rss/channel/link"/>
                        </xsl:attribute> 
                        <xsl:value-of select="rss/channel/link"/>    
                    </a>
                </h1>
                <table border="1">
                    <tr>
                        <th width="30">Title</th>
                        <th width="20">Publication Date</th>
                    </tr>
                    <xsl:for-each select="rss/channel/item">
                        <tr>
                            <td> 
                                <a>  
                                    <xsl:attribute name="href">
                                        <xsl:value-of select="link"/>
                                    </xsl:attribute> 
                                    <xsl:value-of select="title"/>
                                </a> 
                            </td>
                            <td>
                                <xsl:value-of select="pubDate"/> 
                             </td>
                        </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>
