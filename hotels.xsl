<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : hotels.xsl
    Created on : December 12, 2012, 5:24 AM
    Author     : Chowwanat  Wikkanesrungsun
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" 
                xmlns:kml="http://www.opengis.net/kml/2.2">
    
    <xsl:output method="html"/>
    
    <xsl:template match="//kml:Document">
    
        <html>
            <head>
                <title>hotels.xsl</title>
            </head>
            
            <body>
                <h1>
                    <img src="{kml:Style/kml:IconStyle/kml:Icon/kml:href}"/>
                    <b>
                        <xsl:value-of select="kml:name"/>
                    </b>
                        
                    <tr></tr>
                </h1>       
                    <tr>
                        <xsl:text>List of hotels</xsl:text>
                    </tr>
                          
                <ul>
                    <xsl:for-each select="kml:Placemark">
                        <xsl:sort select="kml:name"/>
                        <li>
                            <xsl:value-of select="kml:name"/>
                            <ul>
                                <li>
                                    <a href="{substring-after(substring-before(kml:description,'&#34;&gt;&lt;'),'&lt;a href=&#34;')}">
                                        <xsl:value-of select="substring-after(substring-before(kml:description,'&#34;&gt;&lt;'),'&lt;a href=&#34;')"/>
                                    </a>
                                </li>
                                <li>
                                    Coordinates:
                                    <xsl:value-of select="kml:Point/kml:coordinates"/>
                                </li>
                            </ul>
                        </li>
                    </xsl:for-each>
                </ul>
                        
                        
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>
