<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : MSN.xsl
    Created on : December 11, 2012, 1:22 PM
    Author     : Chowwanat Wikkanesrungsun
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html"/>

    <xsl:template match="/">
        <html>
            <head>
                <title>MSN.xsl</title>
                <style type="text/css">
                    body
                    {
                    font-family: Verdana, arial,sans-serif;
                    }
                </style>
            </head>
            <body>
                <span style = "color: red">[Conversation started on
                    
                    <xsl:value-of select = "Log/Message[1]/@Date"/> 
                    <xsl:text>  </xsl:text>
                    <xsl:value-of select = "Log/Message[1]/@Time"/> 
                    <xsl:text>]</xsl:text>   
                </span>
                <table border = "0">
                    <tr>
                        <td>[
                            <xsl:value-of select = "Log/Message[1]/@Date"/> 
                            <xsl:text>  </xsl:text>
                            <xsl:value-of select = "Log/Message[1]/@Time"/>]
                        </td>
                        <td>
                            <span style = "color : #FFAA00">
                                <xsl:value-of select = "Log/Message[1]/From/User/@FriendlyName"/>
                            </span>
                        </td>
                        <td>
                            <xsl:text>: </xsl:text>
                        </td>
                        <td>
                            <span style = "color : #FFAA00">
                                <xsl:value-of select = "Log/Message[1]/Text"/> 
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td>[
                            <xsl:value-of select = "Log/Message[2]/@Date"/> 
                            <xsl:text>  </xsl:text>
                            <xsl:value-of select = "Log/Message[2]/@Time"/>]
                        </td>
                        <td>
                            <span style = "color : #24913C">
                                <xsl:value-of select = "Log/Message[2]/From/User/@FriendlyName"/>
                            </span>
                        </td>
                        <td>
                            <xsl:text>: </xsl:text>
                        </td>
                        <td>
                            <span style = "color : #24913C">
                                <xsl:value-of select = "Log/Message[2]/Text"/>  
                            </span>
                                
                        </td>
                    </tr>
                    <tr>
                        <td>[
                            <xsl:value-of select = "Log/Message[3]/@Date"/> 
                            <xsl:text>  </xsl:text>
                            <xsl:value-of select = "Log/Message[3]/@Time"/>]
                        </td>
                        <td>
                            <span style = "color : #FFAA00">
                                <xsl:value-of select = "Log/Message[3]/From/User/@FriendlyName"/>
                            </span>
                        </td>
                        <td>
                            <xsl:text>: </xsl:text>
                        </td>
                        <td>
                            <span style = "color : #FFAA00">
                                <xsl:value-of select = "Log/Message[3]/Text"/> 
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td>[
                            <xsl:value-of select = "Log/Message[4]/@Date"/> 
                            <xsl:text>  </xsl:text>
                            <xsl:value-of select = "Log/Message[4]/@Time"/>]
                        </td>
                        <td>
                            <span style = "color : #24913C">
                                <xsl:value-of select = "Log/Message[4]/From/User/@FriendlyName"/>
                            </span>
                        </td>
                        <td>
                            <xsl:text>: </xsl:text>
                        </td>
                        <td>
                            <span style = "color : #24913C">
                                <xsl:value-of select = "Log/Message[4]/Text"/>  
                            </span>   
                        </td>
                    </tr>
                </table>
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>